# Computer multitool bot for Telegram
An easy-to-use Telegram bot to automate computer functions made with Python.

# Capabilities
Currently, this bot is capable of:
- Check if the user is allowed to use the bot (with a given list of allowed users).
- Basic start message to show its commands.
- Make an screenshot and send it instantly to the current chat.
- Power off the host computer:
    - Instantly.
    - In a given amount of time.

# Requirements
- Windows (not tested on GNU/Linux or MacOS).
- Python and the following libraries:
    - `python-telegram-bot`: Basic tools for a Telegram bot.
    - `requests`: For HTTP petitions.
    - `pyscreenshot`: For making screenshots
    - `os, time`: Python-integrated libraries for basic functions.

# Usage
1. Download or clone this repository.
2. Go to the folder of the repository and open a terminal on the current path.
3. Run `python tlgrm_sender.py`

# Roadmap
This project is still under development. I plan to add more functions.
