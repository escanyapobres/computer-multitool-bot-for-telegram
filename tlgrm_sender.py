# -*- coding: utf-8 -*-
"""
Created on Sat Sep 10 12:41:54 2022

@author: aurocta
"""

#!/usr/bin/env python3

from telegram import *
from telegram.ext import *

import pyscreenshot as ImageGrab
import os
import time
import requests

# Token id:
updater = Updater(token='5617990701:AAG7b3CmS5ttlbF_WQSnquRqL8zcCg2ZIl0')
dispatcher = updater.dispatcher

# %% Bot functions
# List of allowed users
allowedUsernames = ["ausiasrt", "Simbalg"]
NAME, OPTION = range(2)

# To check is allowed to use this bot
def userchk(update, context):
	if update.message.from_user['username'] not in allowedUsernames:
		context.bot.send_message(chat_id=update.effective_chat.id, text="You are not allowed to use this bot")
		return False
	else:
		return True

# Start command
def start(update: Update, context: CallbackContext):
	context.bot.send_message(chat_id=update.effective_chat.id, text="Send a /capture request to show your bot's desktop.")
	print('Start.')

# Make and send an screenshot
def capture(update: Update, context: CallbackContext):
	# Check if the user is allowed to use this bot
	if userchk(update, context) is False:
		return
	
	# Get and send an screenshot
	print("New screenshot in one second.")
	time.sleep(1)
	im = ImageGrab.grab()
	im.save("screen.png", "PNG")
	print("Sending image to "+update.message.from_user['username']+"...")
	context.bot.send_photo(update.effective_chat.id, photo=open('screen.png', 'rb'))
	print('Done.')

def poweroff(update: Update, context: CallbackContext):
	context.bot.send_message(chat_id=update.effective_chat.id, text="Do you want to turn off your computer? Press /options to continue.")
	
	return NAME

def options(update, context):
	update.message.reply_text('Set 0 to shutdown now, -1 to cancel and write a number to shutdown in X minutes.')
	return OPTION

def shooter(update, context):
	option = int(update.message.text)
# 	update.message.reply_text(f'Selected option: {option}')
	if option == -1:
		context.bot.send_message(chat_id=update.effective_chat.id, text="Operation cancelled")
	if option == 0:
		context.bot.send_message(chat_id=update.effective_chat.id, text="Your computer will shut down now.")
		os.system("shutdown /s /t 0")
	if option > 0:
		context.bot.send_message(chat_id=update.effective_chat.id, text=f"Your computer will shut down in {option} minutes.")
		os.system("shutdown /s /t "+str(option))
	return ConversationHandler.END

# 	while type(msg)!=int:
# 		time.sleep(1)
# 		print(msg)
# 		msg = update.message.text
# 	else:
# 		update.message.reply_text(f'You answered: {update.message.text}')

conv_handler = ConversationHandler(
		entry_points=[CommandHandler('poweroff', poweroff)],
		fallbacks=[],

		states={
			NAME: [CommandHandler('options', options)],
			OPTION: [MessageHandler(Filters.text, shooter)],
		},
	)

	
# %% Dispatcher

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

capture_handler = CommandHandler('capture', capture)
dispatcher.add_handler(capture_handler)

# poweroff_handler = CommandHandler('poweroff', poweroff)
# dispatcher.add_handler(poweroff_handler)

dispatcher.add_handler(conv_handler)

# dispatcher.add_handler(MessageHandler(Filters.text, messageHandler))
# updater.dispatcher.add_handler(CommandHandler('capture', capture_handler))
print("Running bot now.")
updater.start_polling()
updater.idle()